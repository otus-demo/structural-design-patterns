﻿namespace ProxyPattern.Infrastructure
{
    public class TransactionItem
    {
        public decimal Amount { get; set; }
    }
}
