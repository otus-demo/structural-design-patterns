﻿namespace ProxyPattern.Infrastructure
{
    public enum AcceptanceLevel
    {
        Level1, Level2, Level3
    }
}
