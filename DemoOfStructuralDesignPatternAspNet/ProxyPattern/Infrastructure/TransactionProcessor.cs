﻿namespace ProxyPattern.Infrastructure
{
    public class TransactionProcessor : ITransactionProcessor
    {
        public async Task<string> ProcessAsync(TransactionItem transactionItem)
        {
            // complex code responsible for processing...
            return "Transaction item has been processed!";
        }
    }
}
