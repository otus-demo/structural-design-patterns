﻿namespace ProxyPattern.Infrastructure
{
    public interface ITransactionProcessor
    {
        Task<string> ProcessAsync(TransactionItem transactionItem);
    }
}
