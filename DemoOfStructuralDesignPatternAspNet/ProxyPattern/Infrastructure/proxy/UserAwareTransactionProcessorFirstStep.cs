﻿namespace ProxyPattern.Infrastructure.proxy
{
    [Obsolete("This class is obsolete. Using will produce error", true)]
    public class UserAwareTransactionProcessorFirstStep : ITransactionProcessor
    {
        private readonly ITransactionProcessor _transactionProccessor;

        public UserAwareTransactionProcessorFirstStep(ITransactionProcessor transactionProccessor)
        {
            _transactionProccessor = transactionProccessor;
        }

        public async Task<string> ProcessAsync(TransactionItem transactionItem)
        {
           return await _transactionProccessor.ProcessAsync(transactionItem);
        }
    }
}
