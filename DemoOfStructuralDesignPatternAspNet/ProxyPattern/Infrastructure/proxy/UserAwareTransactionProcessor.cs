﻿namespace ProxyPattern.Infrastructure.proxy
{
    public class UserAwareTransactionProcessor : ITransactionProcessor
    {
        private readonly ITransactionProcessor _transactionProcessor;
        private readonly IUserContext _userContext;

        public UserAwareTransactionProcessor(
            TransactionProcessor transactionProcessor,
            IUserContext userContext)
        {
            _transactionProcessor = transactionProcessor;
            _userContext = userContext;
        }

        public async Task<string> ProcessAsync(TransactionItem transactionItem)
        {
            if (_userContext.CurrentUser.AcceptanceLevel < AcceptanceLevel.Level2)
            {
                return "Current user has not sufficient privileges";
            }

            return await _transactionProcessor.ProcessAsync(transactionItem);
        }
    }
}
