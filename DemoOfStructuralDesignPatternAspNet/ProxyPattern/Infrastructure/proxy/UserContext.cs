﻿namespace ProxyPattern.Infrastructure.proxy
{
    public class UserContext : IUserContext
    {
        public AppUser CurrentUser => new() { AcceptanceLevel = AcceptanceLevel.Level1 };
    }
}
