﻿namespace ProxyPattern.Infrastructure.proxy
{
    public interface IUserContext
    {
        AppUser CurrentUser { get; }
    }
}
