using ProxyPattern.Infrastructure;
using ProxyPattern.Infrastructure.proxy;
using ProxyPattern.Middleware;

var builder = WebApplication.CreateBuilder(args);
#region WithoutProxy
//builder.Services.AddTransient<ITransactionProcessor, TransactionProcessor>();
#endregion

#region Proxy
builder.Services.AddTransient<IUserContext, UserContext>();
builder.Services.AddTransient<TransactionProcessor>();
//builder.Services.AddTransient<ITransactionProcessor, UserAwareTransactionProcessor>(serviceProvider =>
//{
//    var userContext = serviceProvider.GetRequiredService<IUserContext>();
//    var transactionProcessor = serviceProvider.GetRequiredService<TransactionProcessor>();
//    return new UserAwareTransactionProcessor(transactionProcessor, userContext);
//});
builder.Services.AddTransient<ITransactionProcessor, UserAwareTransactionProcessor>();
#endregion



var app = builder.Build();

app.UseMiddleware<ClientCodeMiddleware>();

app.Run();
