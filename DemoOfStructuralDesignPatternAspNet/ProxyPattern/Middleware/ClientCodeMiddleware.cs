﻿using ProxyPattern.Infrastructure;

namespace ProxyPattern.Middleware
{
    public class ClientCodeMiddleware
    {
        public ClientCodeMiddleware(RequestDelegate next) { }

        public async Task InvokeAsync(HttpContext context, ITransactionProcessor processor)
        {
            var transactionItem = new TransactionItem { Amount = 100 };
            var message = await processor.ProcessAsync(transactionItem);
            await context.Response.WriteAsync(message);
        }
    }
}
