﻿using DecoratorPatter.Infrastructure.Core;
using DecoratorPatter.Infrastructure.Core.Abstraction;
using Microsoft.AspNetCore.Mvc;

namespace DecoratorPatter.Controller
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<Employee>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();


            return employees.ToList();
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<Employee>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();           


            return employee;
        }

        /// <summary>
        /// Удалить сотрудника из репозитория по Id
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<bool>> DeleteEmployeeByIdAsync(Guid id)
        {
            //var employee = await _employeeRepository.GetByIdAsync(id);

            //if (employee == null)
            //    return NotFound();

            var res = await _employeeRepository.DeleteByIdAsync(id);

            return res;
        }

        /// <summary>
        /// Добавить сотрудника в репозиторий
        /// </summary>
        /// <param name="employeeCreateModel"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<bool>> AddEmployeeAsync([FromBody] Employee employee)
        {
            employee.Id = Guid.NewGuid();
            var result=await _employeeRepository.CreateAsync(employee);
            return result;
        }

        /// <summary>
        /// Обновить данные по сотруднику в репозитории
        /// </summary>
        /// <param name="employeeCreateModel"></param>
        /// <returns></returns>
        [HttpPatch]
        public async Task<ActionResult<bool>> UpdateEmployeeAsync([FromBody] Employee employee)
        {
            var result = await _employeeRepository.UpdateAsync(employee);
            return result;
        }


    }
}