﻿using DecoratorPatter.Infrastructure.Core;
using DecoratorPatter.Infrastructure.Core.Abstraction;
using System.Collections.Immutable;



namespace DecoratorPatter.Infrastructure.DataAccess.Data
{
    [Obsolete("This class is obsolete. Using will produce error",true)]
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IDataContext<T> data)
        {
            Data =data.GetDataContexts;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<bool> CreateAsync(T createModel)
        {
            var result = false;
            ((List<T>)Data).Add(createModel);
            result= true;

            return Task.FromResult(result);
        }

        public Task<bool> UpdateAsync(T updateModel)
        {
            var result = false;

            var updatedItem=Data.FirstOrDefault(x=>x.Id == updateModel.Id);

            if (updatedItem!=null)
            {               
                CopyProps(updateModel, updatedItem);
                
                result = true;
            }
         

            return Task.FromResult(result);
        }

        private void CopyProps(T source, T target)
        {
            foreach (var prop in source.GetType().GetProperties()) {
                if (prop.Name != "Id"&& prop.Name!="Roles"&& prop.Name != "FullName")
                {
                    var value=prop.GetValue(source, null);
                    target.GetType().GetProperties().FirstOrDefault(p=>p.Name==prop.Name).SetValue(target, value);
                }
            }
        }

        public Task<bool> DeleteByIdAsync(Guid id)
        {
            var result = false;
            var item= Data.FirstOrDefault(x => x.Id == id);
            if (item != null) { result = true; }
            ((List<T>)Data).Remove(item);
            
            return Task.FromResult(result);
        }
    }
}