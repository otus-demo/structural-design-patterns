﻿using DecoratorPatter.Infrastructure.Core;
using DecoratorPatter.Infrastructure.Core.Abstraction;
using Microsoft.Extensions.Caching.Memory;

namespace DecoratorPatter.Infrastructure.DataAccess.Repository
{
    public class SimpleCachingEmployeeRepository
        : IRepository<Employee>
    {
        private readonly IRepository<Employee> _repository;
        private readonly IMemoryCache _cache;
        private readonly ILogger<SimpleCachingEmployeeRepository> _logger;

        public SimpleCachingEmployeeRepository(
            IRepository<Employee> repository,
            IMemoryCache cache,
            ILogger<SimpleCachingEmployeeRepository> logger)
        {
            _repository = repository;
            _cache = cache;
            _logger = logger;

        }

        public Task<Employee> GetByIdAsync(Guid id)
        {
            string key = $"members-{id}";

            _logger.LogInformation($"{key} trying to find in cache");

            var res = _cache.GetOrCreate(
                key,
                entry =>
                {
                    entry.SetAbsoluteExpiration(TimeSpan.FromMinutes(5));
                    var emp = _repository.GetByIdAsync(id);
                    if (emp.Result != null)
                        _logger.LogWarning($"{emp.Result} had found and cached");

                    return emp;
                });
            if (res.Result == null)
                _logger.LogError($"members-{id} not found in cache also in database");
            else
                _logger.LogWarning($"{res.Result} had found");

            return res;
        }


        public Task<bool> CreateAsync(Employee createEmployeeModel)
        {
            return _repository.CreateAsync(createEmployeeModel);
        }

        public Task<bool> DeleteByIdAsync(Guid id)
        {
            return _repository.DeleteByIdAsync(id);
        }

        public Task<IEnumerable<Employee>> GetAllAsync()
        {
            return _repository.GetAllAsync();
        }


        public Task<bool> UpdateAsync(Employee updateEmployeeModel)
        {
            return _repository.UpdateAsync(updateEmployeeModel);
        }
    }
}
