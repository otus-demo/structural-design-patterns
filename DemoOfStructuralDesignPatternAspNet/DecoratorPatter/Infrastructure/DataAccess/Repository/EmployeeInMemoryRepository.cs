﻿using DecoratorPatter.Infrastructure.Core;
using DecoratorPatter.Infrastructure.Core.Abstraction;



namespace DecoratorPatter.Infrastructure.DataAccess.Data
{
    public class EmployeeInMemoryRepository
        : IRepository<Employee>

    {
        protected IEnumerable<Employee> Data { get; set; }

        public EmployeeInMemoryRepository(IDataContext<Employee> data)
        {
            Data = data.GetDataContexts;
        }

        public Task<IEnumerable<Employee>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<Employee> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<bool> CreateAsync(Employee createModel)
        {
            var result = false;
            ((List<Employee>)Data).Add(createModel);
            result = true;

            return Task.FromResult(result);
        }

        public Task<bool> UpdateAsync(Employee updateModel)
        {
            var result = false;

            var updatedItem = Data.FirstOrDefault(x => x.Id == updateModel.Id);

            if (updatedItem != null)
            {
                CopyProps(updateModel, updatedItem);

                result = true;
            }


            return Task.FromResult(result);
        }

        private void CopyProps(Employee source, Employee target)
        {
            foreach (var prop in source.GetType().GetProperties())
            {
                if (prop.Name != "Id" && prop.Name != "Roles" && prop.Name != "FullName")
                {
                    var value = prop.GetValue(source, null);
                    target.GetType().GetProperties().FirstOrDefault(p => p.Name == prop.Name).SetValue(target, value);
                }
            }
        }

        public Task<bool> DeleteByIdAsync(Guid id)
        {
            var result = false;
            var item = Data.FirstOrDefault(x => x.Id == id);
            if (item != null)
            {
                ((List<Employee>)Data).Remove(item);
                result = true;
            }

            return Task.FromResult(result);
        }
    }
}