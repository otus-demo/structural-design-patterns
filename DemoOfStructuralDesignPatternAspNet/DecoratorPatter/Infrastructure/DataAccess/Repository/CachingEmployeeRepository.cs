﻿using DecoratorPatter.Infrastructure.Core;
using DecoratorPatter.Infrastructure.Core.Abstraction;
using Microsoft.Extensions.Caching.Memory;

namespace DecoratorPatter.Infrastructure.DataAccess.Repository
{
    [Obsolete("This class is obsolete. Using will produce error", true)]
    public class CachingEmployeeRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        private readonly IRepository<T> _repository;
        private readonly IMemoryCache _cache;
        private readonly ILogger<CachingEmployeeRepository<T>> _logger;

        public CachingEmployeeRepository(
            IRepository<T> repository,
            IMemoryCache cache,
            ILogger<CachingEmployeeRepository<T>> logger)
        {
            _repository = repository;
            _cache = cache;
            _logger=logger;
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            string key = $"members-{id}";

            _logger.LogInformation($"{key} trying to find in cache");

            var res = _cache.GetOrCreate(
                key,
                entry =>
                {
                    entry.SetAbsoluteExpiration(TimeSpan.FromMinutes(5));
                    var emp = _repository.GetByIdAsync(id);
                    if (emp.Result != null)
                        _logger.LogWarning($"{emp.Result} had found and cached");

                    return emp;
                });
            if (res.Result == null)
                _logger.LogError($"members-{id} not found");
            else
                _logger.LogWarning($"{res.Result} had found");

            return res;
        }


        public Task<bool> CreateAsync(T createEmployeeModel)
        {
           return _repository.CreateAsync(createEmployeeModel);
        }

        public Task<bool> DeleteByIdAsync(Guid id)
        {
            return _repository.DeleteByIdAsync(id);
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
           return _repository.GetAllAsync();
        }
        

        public Task<bool> UpdateAsync(T updateEmployeeModel)
        {
            return _repository.UpdateAsync(updateEmployeeModel);
        }
    }
}
