﻿namespace DecoratorPatter.Infrastructure.Core
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}