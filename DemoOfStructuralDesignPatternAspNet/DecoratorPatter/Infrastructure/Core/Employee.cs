﻿using DecoratorPatter.Infrastructure.Core.Abstraction;

namespace DecoratorPatter.Infrastructure.Core;

public class Employee
    : BaseEntity, IEmployee
{
    public string FirstName { get; set; }
    public string LastName { get; set; }

    public string FullName => $"{FirstName} {LastName}";

    public string Email { get; set; }

    public override string ToString()
    {
        return FirstName+ " "+LastName;
    }
}