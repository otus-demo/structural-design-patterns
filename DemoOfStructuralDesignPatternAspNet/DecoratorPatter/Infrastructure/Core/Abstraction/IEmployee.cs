﻿namespace DecoratorPatter.Infrastructure.Core.Abstraction
{
    public interface IEmployee
    {
        string Email { get; set; }
        string FirstName { get; set; }
        string FullName { get; }
        string LastName { get; set; }
    }
}