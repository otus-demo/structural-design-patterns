﻿namespace DecoratorPatter.Infrastructure.Core.Abstraction;

public interface IRepository<T>
    where T: BaseEntity
{
    Task<IEnumerable<T>> GetAllAsync();
    
    Task<T> GetByIdAsync(Guid id);

    Task<bool> CreateAsync(T createTModel);
    Task<bool> UpdateAsync(T updateTModel);
    Task<bool> DeleteByIdAsync(Guid id);   
}