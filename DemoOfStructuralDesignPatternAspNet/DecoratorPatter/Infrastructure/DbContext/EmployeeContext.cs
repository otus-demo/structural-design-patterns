﻿using DecoratorPatter.Infrastructure.Core;
using DecoratorPatter.Infrastructure.Core.Abstraction;
using DecoratorPatter.Infrastructure.DataAccess.Data;

namespace DecoratorPatter.Infrastructure.DbContext
{
    public class EmployeeContext : IDataContext<Employee>
    {
        private readonly List<Employee> _employee;
        public List<Employee> GetDataContexts => _employee;
        public EmployeeContext() {
            _employee = new List<Employee>(FakeDataFactory.Employees);
        }
    }
}
