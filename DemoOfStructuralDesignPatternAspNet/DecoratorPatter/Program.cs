using DecoratorPatter.Infrastructure.Core;
using DecoratorPatter.Infrastructure.Core.Abstraction;
using DecoratorPatter.Infrastructure.DataAccess.Data;
using DecoratorPatter.Infrastructure.DataAccess.Repository;
using DecoratorPatter.Infrastructure.DbContext;
using Microsoft.Extensions.Caching.Memory;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddMemoryCache();

builder.Services.AddSingleton(typeof(IDataContext<Employee>), typeof(EmployeeContext));

#region without_Decorator
builder.Services.AddScoped(typeof(IRepository<Employee>), typeof(EmployeeInMemoryRepository));
#endregion

#region with_decorator
//builder.Services.AddScoped<IRepository<Employee>>(provider =>
//{
//    var context = provider.GetService<IDataContext<Employee>>();
//    var cache = provider.GetService<IMemoryCache>();
//    var logger = provider.GetService<ILogger<SimpleCachingEmployeeRepository>>();

//    return new SimpleCachingEmployeeRepository(
//         new EmployeeInMemoryRepository(context),
//         cache,
//         logger);
//});
#endregion 


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.MapControllers();

app.Run();
