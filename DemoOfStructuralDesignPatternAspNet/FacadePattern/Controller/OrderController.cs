﻿using FacadePattern.Services;
using Microsoft.AspNetCore.Mvc;

namespace FacadePattern.Controller
{
    [ApiController]
    [Route("api/[controller]")]
    public class OrderController : ControllerBase
    {
        private readonly OrderFacade _orderFacade;

        public OrderController(OrderFacade orderFacade)
        {
            _orderFacade = orderFacade;
        }

        [HttpPost("place-order")]
        public IActionResult PlaceOrder(int productId, decimal amount)
        {
            bool success = _orderFacade.PlaceOrder(productId, amount);
            if (success)
            {
                return Ok("Order placed successfully.");
            }
            return BadRequest("Failed to place the order.");
        }
    }
}
