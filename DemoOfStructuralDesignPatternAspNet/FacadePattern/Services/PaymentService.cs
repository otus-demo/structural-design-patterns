﻿namespace FacadePattern.Services
{
    public class PaymentService
    {
        public bool ProcessPayment(decimal amount)
        {
            // Dummy logic
            Console.WriteLine($"Processing payment of ${amount}...");
            return true;
        }
    }
}
