﻿namespace FacadePattern.Services
{
    public class InventoryService
    {
        public bool CheckInventory(int productId)
        {
            // Dummy logic
            Console.WriteLine($"Checking inventory for product {productId}...");
            return true;
        }
    }
}
