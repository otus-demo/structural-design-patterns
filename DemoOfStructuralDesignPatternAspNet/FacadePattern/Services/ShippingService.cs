﻿namespace FacadePattern.Services
{
    public class ShippingService
    {
        public bool ShipProduct(int productId)
        {
            // Dummy logic
            Console.WriteLine($"Shipping product {productId}...");
            return true;
        }
    }
}
