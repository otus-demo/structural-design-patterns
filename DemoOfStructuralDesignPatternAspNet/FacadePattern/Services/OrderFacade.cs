﻿namespace FacadePattern.Services
{
    public class OrderFacade
    {
        private readonly InventoryService _inventoryService;
        private readonly PaymentService _paymentService;
        private readonly ShippingService _shippingService;

        public OrderFacade(InventoryService inventoryService, PaymentService paymentService, ShippingService shippingService)
        {
            _inventoryService = inventoryService;
            _paymentService = paymentService;
            _shippingService = shippingService;
        }

        public bool PlaceOrder(int productId, decimal amount)
        {
            // Use the individual services through the facade
            Console.WriteLine("Placing order...");

            if (!_inventoryService.CheckInventory(productId))
            {
                Console.WriteLine("Inventory check failed.");
                return false;
            }

            if (!_paymentService.ProcessPayment(amount))
            {
                Console.WriteLine("Payment failed.");
                return false;
            }

            if (!_shippingService.ShipProduct(productId))
            {
                Console.WriteLine("Shipping failed.");
                return false;
            }

            Console.WriteLine("Order placed successfully.");
            return true;
        }
    }
}
