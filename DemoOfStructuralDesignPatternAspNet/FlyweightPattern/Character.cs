﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlyweightPattern
{
    // Concrete flyweight class
    public class Character : ICharacter
    {
        private readonly char _symbol;
        private readonly int _size;
        private readonly string _font;

        public Character(char symbol, int size, string font)
        {
            _symbol = symbol;
            _size = size;
            _font = font;
        }

        public void Display()
        {
            Console.WriteLine($"Symbol: {_symbol}, Size: {_size}, Font: {_font}");
        }
    }
}
