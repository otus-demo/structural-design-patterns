﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlyweightPattern
{
    // Flyweight factory class
    public class CharacterFactory
    {
        private readonly Dictionary<string, ICharacter> _characters = new();

        public ICharacter GetCharacter(char symbol, int size, string font)
        {
            var key = symbol.ToString() + size.ToString() + font;

            if (!_characters.ContainsKey(key))
            {
                _characters.Add(key, new Character(symbol, size, font));
            }

            return _characters[key];
        }
    }
}
