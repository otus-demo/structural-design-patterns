﻿using Microsoft.AspNetCore.Mvc;

namespace FlyweightPatternWebApi
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly UserRoleFactory _roleFactory;

        public UserController(UserRoleFactory roleFactory)
        {
            _roleFactory = roleFactory;
        }

        [HttpPost("assign-role")]
        public IActionResult AssignRole(string userName, string roleName)
        {
            var role = _roleFactory.GetRole(roleName);
            role.AssignUser(userName);
            return Ok($"User {userName} assigned to role {roleName}");
        }
    }
}
