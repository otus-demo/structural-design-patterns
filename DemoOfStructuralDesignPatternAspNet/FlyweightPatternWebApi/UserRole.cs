﻿namespace FlyweightPatternWebApi
{
    public class UserRole
    {
        public string RoleName { get; }

        public UserRole(string roleName)
        {
            RoleName = roleName;
        }

        public void AssignUser(string userName)
        {
            // Simulating some operation on a user with this role
            Console.WriteLine($"User {userName} assigned to role {RoleName}");
        }
    }
}
