﻿namespace FlyweightPatternWebApi
{
    public class UserRoleFactory
    {
        private readonly Dictionary<string, UserRole> _roles = new Dictionary<string, UserRole>();

        public UserRole GetRole(string roleName)
        {
            if (!_roles.ContainsKey(roleName))
            {
                // If the role does not exist, create and store it
                _roles[roleName] = new UserRole(roleName);
                Console.WriteLine($"Created new role: {roleName}");
            }
            else
            {
                // Use existing role
                Console.WriteLine($"Reusing existing role: {roleName}");
            }

            return _roles[roleName];
        }
    }
}
