﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompositePatternFileExplorer
{
    public class FileComponent : IFileSystemComponent
    {
        public string Name { get; }

        private FileInfo _fileInfo;

        public FileComponent(string filePath)
        {
            _fileInfo = new FileInfo(filePath);
            Name = _fileInfo.Name;
        }

        public long GetSize()
        {
            return _fileInfo.Length; // File size in bytes
        }

        public void Display(string indent = "")
        {
            Console.WriteLine($"{indent}File: {Name}, Size: {GetSize()} bytes");
        }
    }
}
