﻿using CompositePatternFileExplorer;
using System;

namespace FileSystemCompositePattern
{
    class Program
    {
        static void Main(string[] args)
        {
            DirectoryComponent currentDirectory = new DirectoryComponent(Environment.CurrentDirectory);

            while (true)
            {
                Console.WriteLine($"\nCurrent Directory: {currentDirectory.Name}");
                Console.WriteLine("Commands: ls (list), cd <dir> (enter directory), up (go to parent), size (total size), exit");

                string command = Console.ReadLine();

                if (command == "ls")
                {
                    // Display current directory contents
                    currentDirectory.Display();
                }
                else if (command.StartsWith("cd "))
                {
                    // Enter a subdirectory
                    string directoryName = command.Substring(3);
                    DirectoryComponent subDirectory = currentDirectory.GetSubDirectory(directoryName);

                    if (subDirectory != null)
                    {
                        currentDirectory = subDirectory;
                    }
                    else
                    {
                        Console.WriteLine($"Subdirectory '{directoryName}' not found.");
                    }
                }
                else if (command == "up")
                {
                    // Go to the parent directory
                    DirectoryComponent parentDirectory = currentDirectory.GetParentDirectory();

                    if (parentDirectory != null)
                    {
                        currentDirectory = parentDirectory;
                    }
                    else
                    {
                        Console.WriteLine("You are at the root directory.");
                    }
                }
                else if (command == "size")
                {
                    // Display the total size of the current directory
                    long totalSize = currentDirectory.GetSize();
                    Console.WriteLine($"Total size: {totalSize} bytes");
                }
                else if (command == "exit")
                {
                    // Exit the program
                    break;
                }
                else
                {
                    Console.WriteLine("Invalid command.");
                }
            }
        }
    }
}

