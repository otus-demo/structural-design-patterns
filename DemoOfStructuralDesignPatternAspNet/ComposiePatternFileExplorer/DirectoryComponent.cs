﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompositePatternFileExplorer
{
    public class DirectoryComponent : IFileSystemComponent
    {
        public string Name { get; }

        private DirectoryInfo _directoryInfo;
        private List<IFileSystemComponent> _components = new List<IFileSystemComponent>();

        public DirectoryComponent(string directoryPath)
        {
            _directoryInfo = new DirectoryInfo(directoryPath);
            Name = _directoryInfo.Name;

            // Populate the directory with its files and subdirectories
            foreach (var file in _directoryInfo.GetFiles())
            {
                _components.Add(new FileComponent(file.FullName));
            }
            foreach (var directory in _directoryInfo.GetDirectories())
            {
                _components.Add(new DirectoryComponent(directory.FullName));
            }
        }

        public long GetSize()
        {
            long totalSize = 0;
            foreach (var component in _components)
            {
                totalSize += component.GetSize();
            }
            return totalSize;
        }

        public void Display(string indent = "")
        {
            Console.WriteLine($"{indent}Directory: {Name}");
            foreach (var component in _components)
            {
                component.Display(indent + "  ");
            }
        }

        public DirectoryComponent GetSubDirectory(string name)
        {
            foreach (var component in _components)
            {
                if (component is DirectoryComponent directory && directory.Name == name)
                {
                    return directory;
                }
            }
            return null;
        }

        public DirectoryComponent GetParentDirectory()
        {
            return _directoryInfo.Parent != null ? new DirectoryComponent(_directoryInfo.Parent.FullName) : null;
        }
    }
}
