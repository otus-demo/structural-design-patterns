﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompositePatternFileExplorer
{
    public interface IFileSystemComponent
    {
        string Name { get; }
        long GetSize();
        void Display(string indent = "");
    }
}
