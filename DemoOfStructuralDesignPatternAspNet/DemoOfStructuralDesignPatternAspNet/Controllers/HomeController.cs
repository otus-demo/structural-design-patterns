using DemoOfStructuralDesignPatternAspNet.Service;
using Microsoft.AspNetCore.Mvc;

namespace DemoOfStructuralDesignPatternAspNet.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HomeController : ControllerBase
    {
        private readonly IEmployeeService _employeesService;

        public HomeController(IEmployeeService employeesService)
        {
            _employeesService = employeesService;
        }

        [HttpGet("[action]")]
        public IActionResult ListOfEmployees()
        {
            var employees = _employeesService.GetEmployees();

            return Ok(employees);
        }
    }
}