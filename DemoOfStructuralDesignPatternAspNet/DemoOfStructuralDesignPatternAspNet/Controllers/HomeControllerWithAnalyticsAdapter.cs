using DemoOfStructuralDesignPatternAspNet.Service;
using Microsoft.AspNetCore.Mvc;

namespace DemoOfStructuralDesignPatternAspNet.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HomeControllerWithAnalyticsAdapter : ControllerBase
    {
        private readonly IEmployeeService _employeesService;
        private readonly IAnalyticsAdapter _analyticsAdapter;
        private readonly ILogger<HomeControllerWithAnalyticsAdapter> _logger;

        public HomeControllerWithAnalyticsAdapter(IEmployeeService employeesService, IAnalyticsAdapter analyticsAdapter, ILogger<HomeControllerWithAnalyticsAdapter> logger)
        {
            _employeesService = employeesService;
            _analyticsAdapter = analyticsAdapter;
            _logger = logger;
        }

        [HttpGet("[action]")]
        public IActionResult ListOfEmployees()
        {
            var employees = _employeesService.GetEmployees();
            _logger.LogInformation("LIst of employees requested");
            return Ok(employees);
        }

        [HttpPost]
        public IActionResult SubmitEmployees()
        {
            var employees = _employeesService.GetEmployees();

            _logger.LogInformation("List of employees WITH ANALYTICS requested");

            _analyticsAdapter.ProcessEmployees(employees);

            _logger.LogInformation("Make redirection");

            return RedirectToAction("ListOfEmployees");
        }
    }
}