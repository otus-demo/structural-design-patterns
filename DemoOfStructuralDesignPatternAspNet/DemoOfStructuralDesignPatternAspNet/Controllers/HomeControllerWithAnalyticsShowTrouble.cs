using DemoOfStructuralDesignPatternAspNet.Service;
using Microsoft.AspNetCore.Mvc;

namespace DemoOfStructuralDesignPatternAspNet.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HomeControllerWithAnalyticsShowTrouble : ControllerBase
    {
        private readonly IEmployeeService _employeesService;
        private readonly IAnalyticsService _analyticsService;
        private readonly ILogger<HomeControllerWithAnalyticsShowTrouble> _logger;

        public HomeControllerWithAnalyticsShowTrouble(IEmployeeService employeesService, IAnalyticsService analyticsService, ILogger<HomeControllerWithAnalyticsShowTrouble> logger)
        {
            _employeesService = employeesService;
            _analyticsService = analyticsService;
            _logger = logger;
        }

        [HttpGet("[action]")]
        public IActionResult ListOfEmployees()
        {
            var employees = _employeesService.GetEmployees();
            _logger.LogInformation("LIst of employees requested");
            return Ok(employees);
        }

        [HttpPost]
        public IActionResult SubmitEmployees()
        {
            var employees = _employeesService.GetEmployees();

           //_analyticsService.GenerateReport(employees);

            return RedirectToAction("ListOfEmployees");
        }
    }
}