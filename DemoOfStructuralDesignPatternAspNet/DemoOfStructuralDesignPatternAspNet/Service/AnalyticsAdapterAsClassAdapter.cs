﻿using DemoOfStructuralDesignPatternAspNet.Model;

namespace DemoOfStructuralDesignPatternAspNet.Service
{
    public class AnalyticsAdapterAsClassAdapter : AnalyticsService, IAnalyticsAdapter
    {
        public void ProcessEmployees(List<Employee> employees)
        {
            var json = System.Text.Json.JsonSerializer.Serialize(employees);

            GenerateReport(json);
        }
    }
}
