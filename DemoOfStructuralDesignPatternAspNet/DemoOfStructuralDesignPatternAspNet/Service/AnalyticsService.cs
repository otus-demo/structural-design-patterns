﻿namespace DemoOfStructuralDesignPatternAspNet.Service
{
    public class AnalyticsService : IAnalyticsService
    {
        public void GenerateReport(string json)
        {
            // Process json to generate report.
            Console.BackgroundColor = ConsoleColor.Blue;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Some logic of reporter");
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
