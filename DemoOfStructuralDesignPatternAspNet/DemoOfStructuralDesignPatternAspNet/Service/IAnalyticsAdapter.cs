﻿using DemoOfStructuralDesignPatternAspNet.Model;

namespace DemoOfStructuralDesignPatternAspNet.Service
{
    public interface IAnalyticsAdapter
    {
        void ProcessEmployees(List<Employee> employees);
    }
}
