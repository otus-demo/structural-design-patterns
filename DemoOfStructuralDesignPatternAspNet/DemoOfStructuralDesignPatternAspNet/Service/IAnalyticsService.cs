﻿namespace DemoOfStructuralDesignPatternAspNet.Service
{
    public interface IAnalyticsService
    {
        void GenerateReport(string json);
    }
}
