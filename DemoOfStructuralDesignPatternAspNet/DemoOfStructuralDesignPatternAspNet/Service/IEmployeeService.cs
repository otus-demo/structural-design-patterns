﻿using DemoOfStructuralDesignPatternAspNet.Model;

namespace DemoOfStructuralDesignPatternAspNet.Service
{
    public interface IEmployeeService
    {
        List<Employee> GetEmployees();
    }
}
