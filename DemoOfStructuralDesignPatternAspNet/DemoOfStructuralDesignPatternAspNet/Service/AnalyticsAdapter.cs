﻿using DemoOfStructuralDesignPatternAspNet.Model;

namespace DemoOfStructuralDesignPatternAspNet.Service
{
    public class AnalyticsAdapter : IAnalyticsAdapter
    {
        private readonly IAnalyticsService _analyticsService;

        public AnalyticsAdapter(IAnalyticsService analyticsService)
        {
            _analyticsService = analyticsService;
        }
        public void ProcessEmployees(List<Employee> employees)
        {
            var json = System.Text.Json.JsonSerializer.Serialize(employees);

            _analyticsService.GenerateReport(json);
        }
    }
}
