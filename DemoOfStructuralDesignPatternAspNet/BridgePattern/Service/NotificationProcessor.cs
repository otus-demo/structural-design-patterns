﻿namespace BridgePattern.Service
{
    public abstract class NotificationProcessor
    {
        public abstract void ProcessNotification(string message);
    }
}
