﻿namespace BridgePattern.Service
{
    public class TextNotificationSender : TextNotificationProcessor
    {
        public override void ProcessNotification(string message)
        {
            base.ProcessNotification(message);
            // Send message here
            Console.BackgroundColor = ConsoleColor.Green;
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine($"Text: {notificationMessage}");
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
