﻿namespace BridgePattern.Service
{
    public class EmailNotificationSender : EmailNotificationProcessor
    {
        public override void ProcessNotification(string message)
        {
            base.ProcessNotification(message);
            // Send message here
            Console.BackgroundColor = ConsoleColor.Yellow;
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine($"Email: {notificationMessage}");
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
