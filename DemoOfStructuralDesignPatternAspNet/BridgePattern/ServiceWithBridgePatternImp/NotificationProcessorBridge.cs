﻿using BridgePattern.Service;

namespace BridgePattern.ServiceWithBridgePatternImp
{
    public class NotificationProcessorBridge : INotificationProcessorBridge
    {
        private readonly IEnumerable<INotificationSenderBridge> _notificationSender;
        public IEnumerable<INotificationSenderBridge> Sender { get=>_notificationSender;  }
        public string Notification { get ;set; } = "Notification from anywhere)";

        public NotificationProcessorBridge(IEnumerable<INotificationSenderBridge> notificationSender)
        {
            _notificationSender = notificationSender;
        }

        public void Send()
        {
            foreach (var notificationSender in Sender)
            {
                notificationSender.Send(Notification);
            }
            
        }
    }
}
