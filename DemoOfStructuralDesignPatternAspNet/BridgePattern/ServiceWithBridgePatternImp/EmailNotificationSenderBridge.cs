﻿namespace BridgePattern.ServiceWithBridgePatternImp
{
    public class EmailNotificationSenderBridge : INotificationSenderBridge
    {
        public void Send(string message)
        {
            Console.BackgroundColor = ConsoleColor.Magenta;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine($"Email: {message}");
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
