﻿using BridgePattern.Service;

namespace BridgePattern.ServiceWithBridgePatternImp
{
    public class TextNotificationProcessorBridge : INotificationProcessorBridge
    {
        private readonly INotificationSenderBridge _notificationSender;
        public INotificationSenderBridge Sender { get=> _notificationSender;  }
        public string Notification { get ;set; } = "Notification from anywhere)";

        public TextNotificationProcessorBridge(INotificationSenderBridge notificationSender)
        {
            _notificationSender = notificationSender;
        }

        public void Send()
        {
            Sender.Send(Notification);
        }
    }
}
