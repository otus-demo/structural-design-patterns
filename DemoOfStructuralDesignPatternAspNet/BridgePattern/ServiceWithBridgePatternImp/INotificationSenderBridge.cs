﻿namespace BridgePattern.ServiceWithBridgePatternImp
{
    /// <summary>
    /// Channel to send notification messages
    /// </summary>
    public interface INotificationSenderBridge
    {
        /// <summary>
        /// Method to send some information over the channel
        /// </summary>
        /// <param name="textToSend"></param>
        void Send(string textToSend);
    }
}
