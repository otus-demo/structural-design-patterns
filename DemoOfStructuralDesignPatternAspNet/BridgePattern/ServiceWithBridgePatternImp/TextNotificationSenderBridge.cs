﻿namespace BridgePattern.ServiceWithBridgePatternImp
{
    public class TextNotificationSenderBridge : INotificationSenderBridge
    {
        public void Send(string message)
        {           
            Console.BackgroundColor = ConsoleColor.Blue;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine($"TextNotificationSender is sending  message : {message}");
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
