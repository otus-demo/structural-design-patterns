﻿using BridgePattern.Service;

namespace BridgePattern.ServiceWithBridgePatternImp
{
    public class EmailNotificationProcessorBridge : INotificationProcessorBridge
    {
        private readonly INotificationSenderBridge _notificationSender;

        public EmailNotificationProcessorBridge(INotificationSenderBridge notificationSender)
        {
            this._notificationSender = notificationSender;
        }

        public INotificationSenderBridge Sender => _notificationSender;

        public string Notification { get; set; } = "Some message";



        public void Send()
        {
            //could be some processing here before sending
            _notificationSender.Send($"<html>{Notification}</html>");
        }
    }
}
