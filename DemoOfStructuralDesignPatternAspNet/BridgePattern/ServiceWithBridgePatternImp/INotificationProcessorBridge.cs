﻿namespace BridgePattern.ServiceWithBridgePatternImp
{
    public interface INotificationProcessorBridge
    {
        /// <summary>
        /// Bridge to the other abstraction
        /// </summary>
        IEnumerable<INotificationSenderBridge> Sender { get; }
        /// <summary>
        /// Data to process
        /// </summary>
        string Notification { get; set; }
        /// <summary>
        /// Method to process notification and send using bridge abstraction
        /// </summary>
        void Send();
        
    }
}
