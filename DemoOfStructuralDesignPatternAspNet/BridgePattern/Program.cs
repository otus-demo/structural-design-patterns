using BridgePattern.Service;
using BridgePattern.ServiceWithBridgePatternImp;
using Microsoft.AspNetCore.Mvc;
using System.Web.Http;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

#region register_without_bridge
builder.Services.AddTransient<NotificationProcessor, TextNotificationSender>();
builder.Services.AddTransient<NotificationProcessor, EmailNotificationSender>();
#endregion 

#region register_with_bridge
builder.Services.AddTransient<INotificationProcessorBridge, NotificationProcessorBridge>();
builder.Services.AddTransient<INotificationSenderBridge, TextNotificationSenderBridge>();
builder.Services.AddTransient<INotificationSenderBridge, EmailNotificationSenderBridge>();
#endregion


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

#region without_bridge
app.MapPost("/notification", (NotificationProcessor notificationProcessor, [FromUri] string message) =>
{
    foreach(var notificationProc in app.Services.GetServices(typeof(NotificationProcessor)))
    {
        ((NotificationProcessor)notificationProc).ProcessNotification(message);
    }
    //notificationProcessor.ProcessNotification(message);
    return Results.Ok();
})
.WithName("Notification");
#endregion

#region with_bridge
app.MapPost("/notificationBridge", (INotificationProcessorBridge notificationProcessor, [System.Web.Http.FromBody] string message) =>
{
    notificationProcessor.Notification = message;
    notificationProcessor.Send();
    return Results.Ok();
})
.WithName("NotificationBridge");


#endregion


app.Run();